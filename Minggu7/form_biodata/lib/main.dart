import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    home: new Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<String> agama = [
    'Islam',
    'Kristen',
    'Katolik',
    'Hindu',
    'Budha',
    'Khonghucu'
  ];
  String _agama = 'Islam';
  String _jk = '';

  TextEditingController controllerNama = TextEditingController();
  TextEditingController controllerPass = TextEditingController();
  TextEditingController controllerMoto = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Form Biodata'),
        leading: Icon(Icons.list),
        backgroundColor: Colors.blue,
      ),
      body: ListView(children: [
        Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              TextField(
                controller: controllerNama,
                decoration: InputDecoration(
                  hintText: 'Nama Lengkap',
                  labelText: 'Nama Lengkap',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              TextField(
                controller: controllerPass,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: 'Password',
                  labelText: 'Password',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              TextField(
                controller: controllerMoto,
                maxLines: 3,
                decoration: InputDecoration(
                  hintText: 'Moto hidup',
                  labelText: 'Moto hidup',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              RadioListTile(
                value: 'Laki-laki',
                title: Text('Laki-Laki'),
                groupValue: _jk,
                onChanged: (String? value) {
                  _pilihJk(value!);
                },
                activeColor: Colors.blue,
                subtitle: Text('Pilih jika anda laki-laki'),
              ),
              RadioListTile(
                value: 'Perempuan',
                title: Text('Perempuan'),
                groupValue: _jk,
                onChanged: (String? value) {
                  _pilihJk(value!);
                },
                activeColor: Colors.blue,
                subtitle: Text('Pilih jika anda perempuan'),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              Row(
                children: <Widget>[
                  Text(
                    'Agama',
                    style: TextStyle(fontSize: 18.0, color: Colors.blue),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                  ),
                  DropdownButton(
                    value: _agama,
                    items: agama.map((String value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (String? value) {
                      setState(
                        () {
                          _pilihAgama(value!);
                        },
                      );
                    },
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              RaisedButton(
                child: Text(
                  'Simpan',
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                onPressed: () {
                  kirimData();
                },
              ),
            ],
          ),
        ),
      ]),
    );
  }

  void _pilihJk(String value) {
    setState(() {
      _jk = value;
    });
  }

  void _pilihAgama(String value) {
    setState(() {
      _agama = value;
    });
  }

  void kirimData() {
    AlertDialog alertDialog = AlertDialog(
      content: Container(
        height: 200.0,
        child: Column(
          children: <Widget>[
            Text("Nama Lengkap : ${controllerNama.text}"),
            Text("Password : ${controllerPass.text}"),
            Text("Moto Hidup : ${controllerMoto.text}"),
            Text("Jenis Kelamin : $_jk"),
            Text("Agama : $_agama"),
            RaisedButton(
              child: Text(
                'OK',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => Navigator.pop(context),
              color: Colors.blue,
            ),
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }
}
