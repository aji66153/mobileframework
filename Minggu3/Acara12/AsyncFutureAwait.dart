void main(List<String> args) async {
  Person person = new Person();

  print('job 1');
  print('job 2');
  await person.getDataAsync();
  print('job 3 :' + person.name);
  print('job 4');
}

class Person {
  String name = 'default name';

  void getData() {
    name = 'Joko';
    print('get data [done]');
  }

  Future<void> getDataAsync() async {
    await Future.delayed(Duration(seconds: 2));
    name = 'Joko';
    print('get data [done]');
  }
}
