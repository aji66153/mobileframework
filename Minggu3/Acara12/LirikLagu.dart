import 'dart:io';
import 'dart:async';

void main(List<String> args) async {
  var lirik = Lirik();

  print('Lagu akan dimulai dalam hitungan');
  print(await lirik.intro1());
  print(await lirik.intro2());
  print(await lirik.intro3());
  stdout.write('\n');
  print(await lirik.Line1());
  print(await lirik.Line2());
  print(await lirik.Line3());
  print(await lirik.Line4());
  stdout.write('\n');
  print(await lirik.Line5());
  print(await lirik.Line6());
  print(await lirik.Line7());
  print(await lirik.Line8());
}

class Lirik {
  Future<String> intro1() async {
    String lirik = '1';
    return await Future.delayed(Duration(seconds: 2), () => lirik);
  }

  Future<String> intro2() async {
    String lirik = '2';
    return await Future.delayed(Duration(seconds: 2), () => lirik);
  }

  Future<String> intro3() async {
    String lirik = '3';
    return await Future.delayed(Duration(seconds: 2), () => lirik);
  }

  Future<String> Line1() async {
    String lirik = 'Aku wong seng pernah';
    return await Future.delayed(Duration(seconds: 4), () => lirik);
  }

  Future<String> Line2() async {
    String lirik = 'Teko neng uripmu';
    return await Future.delayed(Duration(seconds: 4), () => lirik);
  }

  Future<String> Line3() async {
    String lirik = 'Nanging aku durung iso';
    return await Future.delayed(Duration(seconds: 4), () => lirik);
  }

  Future<String> Line4() async {
    String lirik = 'Dadi pilihanmu';
    return await Future.delayed(Duration(seconds: 4), () => lirik);
  }

  Future<String> Line5() async {
    String lirik = 'Aku wong seng pernah';
    return await Future.delayed(Duration(seconds: 4), () => lirik);
  }

  Future<String> Line6() async {
    String lirik = 'Merjuangke ati';
    return await Future.delayed(Duration(seconds: 4), () => lirik);
  }

  Future<String> Line7() async {
    String lirik = 'Dinggo koe, cah ayu';
    return await Future.delayed(Duration(seconds: 4), () => lirik);
  }

  Future<String> Line8() async {
    String lirik = 'Sing wes tak gemateni';
    return await Future.delayed(Duration(seconds: 4), () => lirik);
  }
}
