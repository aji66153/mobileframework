import 'package:flutter/material.dart';

void main() {
  runApp(const Gradient());
}

class Gradient extends StatefulWidget {
  const Gradient({Key? key}) : super(key: key);

  @override
  State<Gradient> createState() => _GradientState();
}

class _GradientState extends State<Gradient> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Gradient'),
        ),
        body: Center(
          child: Container(
            margin: EdgeInsets.all(20),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Colors.amber, Colors.blue],
              ),
              borderRadius: BorderRadius.circular(20),
            ),
          ),
        ),
      ),
    );
  }
}
