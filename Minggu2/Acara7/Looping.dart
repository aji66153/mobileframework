void main(List<String> args) {
  // while loop
  var start = 1;
  while (start <= 10) {
    print(start);
    start++;
  }

  print('==============================');

  // for loop
  for (var i = 1; i <= 10; i++) {
    print(i);
  }
}
