void main(List<String> args) {
  // print
  print('Hello world!');

  // variabel
  var name = "John"; // Tipe
  var angka = 12;
  var todayIsFriday = false;
  print(name); // "John"
  print(angka); // 12
  print(todayIsFriday); // false

  // operator
  var angka1 = 10;
  var angka2 = 20;

  print(angka1 + angka2);
  print(angka1 - angka2);
  print(angka1 * angka2);
  print(angka1 / angka2);
}
