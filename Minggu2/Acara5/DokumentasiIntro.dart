void main(List<String> args) {
  // string
  var kata = "dart";
  print(kata[0]);
  print(kata[3]);

  print('\n');

  // // numbers
  // // deklarasi integer
  // int num1 = 20;

  // // deklarasi double
  // double num2 = 20.5;

  // // print the values
  // print(num1);
  // print(num2);

  // print('\n');

  // deklarasi type data numbers
  // int dan double
  int num1 = 10;
  double num2 = 10.3;
  print(num1);
  print(num2);
  // konversi int ke string dan sebaliknya
  print(num.parse('100'));
  print(num.parse('20.2'));

  String intToStr = "$num1";
  print('Nilai String : ${intToStr}');
}
