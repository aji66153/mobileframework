import 'dart:convert';
import 'package:http/http.dart' show Client, Response;
import 'package:manifest_asset_activity/model/popular_movies.dart';

class ApiProvider {
  String apiKey = '321686592c8c055f58ffe08d7fed4563';
  String baseUrl = 'https://api.themoviedb.org/3';

  Client client = Client();

  Future<PopularMovies> getPopularMovies() async {
    Response response =
        await client.get('$baseUrl/movie/popular?api_key=$apiKey');
    if (response.statusCode == 200) {
      return PopularMovies.fromJson(jsonDecode(response.body));
    } else {
      throw Exception(response.statusCode);
    }
  }
}
